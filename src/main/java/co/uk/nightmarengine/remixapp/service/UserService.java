package co.uk.nightmarengine.remixapp.service;

import java.util.List;

import co.uk.nightmarengine.remixapp.entity.User;

public interface UserService {

	public List<User> getUsers();
	
	public void saveUser(User user);
	
	public User getUser(int id);
	
	public void deleteUser(int id);
	
}
