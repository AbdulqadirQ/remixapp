package co.uk.nightmarengine.remixapp.entity;

import java.sql.Timestamp;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="Track")
public class Track {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id")
	private int id;
	
	@Column(name="title")
	private String title;
	
	@ManyToOne(fetch=FetchType.LAZY, 
			   cascade= { CascadeType.PERSIST, CascadeType.MERGE,
			  			  CascadeType.DETACH, CascadeType.REFRESH })		
	@JoinColumn(name="user_id")
	private User user;
	
	@ManyToOne(fetch=FetchType.LAZY, 
			   cascade= { CascadeType.PERSIST, CascadeType.MERGE,
						  CascadeType.DETACH, CascadeType.REFRESH })		
	@JoinColumn(name="artist_id")
	private Artist artist;
	
	@Column(name="genre")
	private String genre;
	
	@Column(name="bpm")
	private int bpm;
	
	@Column(name="youtube_uri")
	private String youtubeUri;
	
	@Column(name="date_added")
	private Timestamp dateAdded;
	
	@Column(name="download_count")
	private int downloadCount;
	
	@Column(name="track_file")
	private String trackFile;
	
	public Track() {
	}	

	public Track(String title, User user, Timestamp dateAdded, String trackFile) {
		super();
		this.title = title;
		this.user = user;
		this.dateAdded = dateAdded;
		this.trackFile = trackFile;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public Artist getArtist() {
		return artist;
	}

	public void setArtist(Artist artist) {
		this.artist = artist;
	}

	public String getGenre() {
		return genre;
	}

	public void setGenre(String genre) {
		this.genre = genre;
	}

	public int getBpm() {
		return bpm;
	}

	public void setBpm(int bpm) {
		this.bpm = bpm;
	}

	public String getYoutubeUri() {
		return youtubeUri;
	}

	public void setYoutubeUri(String youtubeUri) {
		this.youtubeUri = youtubeUri;
	}

	public Timestamp getDateAdded() {
		return dateAdded;
	}

	public void setDateAdded(Timestamp dateAdded) {
		this.dateAdded = dateAdded;
	}

	public int getDownloadCount() {
		return downloadCount;
	}

	public void setDownloadCount(int downloadCount) {
		this.downloadCount = downloadCount;
	}

	public String getTrackFile() {
		return trackFile;
	}

	public void setTrackFile(String trackFile) {
		this.trackFile = trackFile;
	}

	@Override
	public String toString() {
		return "Track [id=" + id + ", title=" + title + ", genre=" + genre + ", dateAdded=" + dateAdded
				+ ", downloadCount=" + downloadCount + "]";
	}	
	
}
