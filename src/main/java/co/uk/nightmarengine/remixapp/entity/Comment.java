package co.uk.nightmarengine.remixapp.entity;

import java.sql.Timestamp;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="Comment")
public class Comment {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column
	private int id;
	
	@ManyToOne(fetch=FetchType.LAZY, 
			   cascade= { CascadeType.PERSIST, CascadeType.MERGE,
			  			  CascadeType.DETACH, CascadeType.REFRESH })		
	@JoinColumn(name="track_id")
	private Track track;

	@ManyToOne(fetch=FetchType.LAZY)		
	@JoinColumn(name="user_id")
	private User user;
	
	@Column(name="content")
	private String content;
	
	@Column(name="time_posted")
	private Timestamp timePosted;
	
	public Comment() {
	}

	public Comment(Track track, User user, String content, Timestamp timePosted) {
		this.track = track;
		this.user = user;
		this.content = content;
		this.timePosted = timePosted;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Track getTrack() {
		return track;
	}

	public void setTrack(Track track) {
		this.track = track;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public Timestamp getTimePosted() {
		return timePosted;
	}

	public void setTimePosted(Timestamp timePosted) {
		this.timePosted = timePosted;
	}

	@Override
	public String toString() {
		return "Comment [id=" + id + ", track=" + track + ", user=" + user + ", content=" + content + "]";
	}
	
	
	
}
