package co.uk.nightmarengine.remixapp.entity;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="Library")
public class Library {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id")
	private int id;
	
	@ManyToOne(fetch=FetchType.LAZY, 
			   cascade= { CascadeType.PERSIST, CascadeType.MERGE,
			  			  CascadeType.DETACH, CascadeType.REFRESH })		
	@JoinColumn(name="track_id")
	private Track track;
	
	@ManyToOne(fetch=FetchType.LAZY,
			   cascade= { CascadeType.PERSIST, CascadeType.MERGE,
			  			  CascadeType.DETACH, CascadeType.REFRESH })		
	@JoinColumn(name="user_id")
	private User user;
	
	public Library() {
	}

	public Library(Track track, User user) {
		this.track = track;
		this.user = user;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Track getTrack() {
		return track;
	}

	public void setTrack(Track track) {
		this.track = track;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	@Override
	public String toString() {
		return "Library [id=" + id + ", track=" + track + ", user=" + user + "]";
	}
		
	
}
