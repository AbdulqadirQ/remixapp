package co.uk.nightmarengine.remixapp.entity;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="Follow")
public class Follow {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id")
	private int id;
	
	@ManyToOne(fetch=FetchType.LAZY, 
			   cascade= { CascadeType.PERSIST, CascadeType.MERGE,
				  		  CascadeType.DETACH, CascadeType.REFRESH })		
	@JoinColumn(name="followed_user_id")
	private User followedUser;
	
	@ManyToOne(fetch=FetchType.LAZY, 
			   cascade= { CascadeType.PERSIST, CascadeType.MERGE,
				  		  CascadeType.DETACH, CascadeType.REFRESH })		
	@JoinColumn(name="following_user_id")
	private User followingUser;
	
	public Follow() {
	}

	public Follow(User followedUser, User followingUser) {
		this.followedUser = followedUser;
		this.followingUser = followingUser;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public User getFollowedUser() {
		return followedUser;
	}

	public void setFollowedUser(User followedUser) {
		this.followedUser = followedUser;
	}

	public User getFollowingUser() {
		return followingUser;
	}

	public void setFollowingUser(User followingUser) {
		this.followingUser = followingUser;
	}

	@Override
	public String toString() {
		return "Follow [id=" + id + ", followedUser=" + followedUser + ", followingUser=" + followingUser + "]";
	}
	
}
