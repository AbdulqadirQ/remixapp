package co.uk.nightmarengine.remixapp.dao;

import java.util.List;

import co.uk.nightmarengine.remixapp.entity.User;

public interface UserDAO {

	public List<User> getUsers();
	
	public void saveUser(User user);
	
	public User getUser(int id);
	
	public void deleteUser(int id);
	
}
