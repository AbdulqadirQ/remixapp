package co.uk.nightmarengine.remixapp.dao;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import co.uk.nightmarengine.remixapp.entity.User;

@Repository
public class UserDAOImpl implements UserDAO {

	@Autowired
	private SessionFactory sessionFactory;
	
	@Override
	public List<User> getUsers() {
		
		Session currentSession = sessionFactory.getCurrentSession();
		Query<User> query = currentSession.createQuery(
				"from User", User.class);
		List<User> users = query.getResultList();
		
		return users;
	}

	@Override
	public void saveUser(User user) {
		// TODO Auto-generated method stub

	}

	@Override
	public User getUser(int id) {
		
		Session session = sessionFactory.getCurrentSession();
		Query<User> query = session.createQuery("from User where id=:userId", 
												User.class);
		query.setParameter("userId", id);
		User user = query.getResultList().get(0);
		
		return user;
	}

	@Override
	public void deleteUser(int id) {
		Session session = sessionFactory.getCurrentSession();
		Query query = session.createQuery("delete from User where id=:userId");
		query.setParameter("userId", id);
		query.executeUpdate();

	}

}
