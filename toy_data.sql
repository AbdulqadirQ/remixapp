INSERT INTO Artist (first_name, last_name, followers, description)
VALUES
('mc','hammer',15,'a cool guy'),
('fire','emblem',32,'a funny guy');

INSERT INTO `User` (username, first_name, last_name, email, `password`, enabled,
					created_at, location, description, picture_file)
VALUES
('jugdeep420', 'jugdeep', 'princeton', 'jug2cool@gmail.com',
'{bcrypt}$2y$12$OpyT0Hvr/ThuW17zMgJOWOr8bxLMtFm0iXqPdyGun9Ciizc8AYxA2', 1,
NOW(), 'UK', 'some douche', 'jugdeep.png'),
('titusB', 'titus', 'bundaro', 'btuggernuts@gmail.com', 
'{bcrypt}$2y$12$OpyT0Hvr/ThuW17zMgJOWOr8bxLMtFm0iXqPdyGun9Ciizc8AYxA2', 1,
NOW(), 'Japan', 'mainest man', 'tuggernuts.png'),
('repositionFodder', 'barst', 'feh', 'barst@gmail.com', 
'{bcrypt}$2y$12$OpyT0Hvr/ThuW17zMgJOWOr8bxLMtFm0iXqPdyGun9Ciizc8AYxA2', 1,
NOW(), 'UK', 'im only good for fodder', 'barst.png');

INSERT INTO Track (title, user_id, artist_id, genre, bpm, youtube_uri,
					date_added, download_count, track_file)
VALUES
('2 legit 2 quit', 1, 1, 'techno', 14, 'v=wiyYozeOoKs',
NOW(), 13465, '2legit2quit.mp4'),
('Road Taken', 2, 2, 'jazz', 603, 'v=OShvQvG_Obw&index=5&list=RDQMck7j05-rCPw',
NOW(), 98, 'RoadTaken.mp4'),
('Resolve', 3, 2, 'jazz', 453, 'v=17tPXr8h1PA&list=RDQMck7j05-rCPw&index=12',
NOW(), 806, 'Resolve.mp4');

INSERT INTO Library (track_id, user_id)
VALUES
(1,1), (1,2), (1,3),
(2,1),
(3,2), (3,3);

INSERT INTO `Comment` (track_id, user_id, content, time_posted)
VALUES
(1, 1, 'wow such a fresh tune wonder who made it', NOW()),
(1, 2, '2 fresh 2 sick - 5 stars from ol titus', NOW()),
(1, 3, 'nah mates not for me some weak shiy', NOW()),
(2,3, 'best tune eu', NOW()),
(2,3, 'best tune na', NOW());

INSERT INTO Follow (followed_user_id, following_user_id)
VALUES
(1,2),
(1,3),
(2,1),
(2,3),
(3,1),
(3,2);