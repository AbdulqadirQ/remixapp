DROP SCHEMA IF EXISTS remix_webapp;
CREATE SCHEMA remix_webapp;

USE remix_webapp;

DROP TABLE IF EXISTS Artist;
CREATE TABLE Artist(
id int(11) NOT NULL auto_increment,
first_name varchar(48) DEFAULT NULL,
last_name varchar(48) DEFAULT NULL,
followers int(11) DEFAULT NULL,
description varchar(512) DEFAULT NULL,
PRIMARY KEY (id)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;

DROP TABLE IF EXISTS `User`;
CREATE TABLE `User`(
id int(11) NOT NULL auto_increment,
username varchar(50) NOT NULL,
`password` varchar(68)  NOT NULL,
enabled tinyint (1) NOT NULL,
first_name varchar(48) DEFAULT NULL,
last_name varchar(48) DEFAULT NULL,
email varchar(128) DEFAULT NULL,
created_at timestamp NOT NULL,
location varchar(48) DEFAULT NULL,
description varchar(512) DEFAULT NULL,
picture_file varchar(256) DEFAULT NULL,
PRIMARY KEY (id),
UNIQUE KEY(username)
)ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;

DROP TABLE IF EXISTS Track;
CREATE TABLE Track(
id int(11) NOT NULL auto_increment,
title varchar(48) NOT NULL,
user_id int(11) DEFAULT NULL,
artist_id int(11) DEFAULT NULL,
genre varchar(48) DEFAULT NULL,
bpm int(11) DEFAULT NULL,
youtube_uri varchar(256) DEFAULT NULL,
date_added timestamp NOT NULL,
download_count int(11) DEFAULT NULL,
track_file varchar(256) NOT NULL,
PRIMARY KEY (id),

CONSTRAINT FK_TrackUser FOREIGN KEY (user_id) 
REFERENCES `User`(id)
ON DELETE SET NULL ON UPDATE NO ACTION,

CONSTRAINT FK_TrackArtist FOREIGN KEY (artist_id) 
REFERENCES Artist(id)
ON DELETE SET NULL ON UPDATE NO ACTION

)ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;

DROP TABLE IF EXISTS Library;
CREATE TABLE Library(
id int(11) NOT NULL auto_increment,
track_id int(11) NOT NULL,
user_id int(11) NOT NULL,
PRIMARY KEY (id),

CONSTRAINT FK_LibraryUser FOREIGN KEY (user_id) 
REFERENCES `User`(id)
ON DELETE CASCADE ON UPDATE NO ACTION,

CONSTRAINT FK_LibraryTrack FOREIGN KEY (track_id) 
REFERENCES Track(id)
ON DELETE CASCADE ON UPDATE NO ACTION

)ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;

DROP TABLE IF EXISTS `Comment`;
CREATE TABLE Comment(
id int(11) NOT NULL auto_increment,
track_id int(11) DEFAULT NULL,
user_id int(11) DEFAULT NULL,
content varchar(512) NOT NULL,
time_posted timestamp NOT NULL,
PRIMARY KEY (id),

CONSTRAINT FK_Comment_Track FOREIGN KEY (track_id) 
REFERENCES Track(id)
ON DELETE SET NULL ON UPDATE NO ACTION,

CONSTRAINT FK_Comment_User FOREIGN KEY (user_id) 
REFERENCES `User`(id)
ON DELETE SET NULL ON UPDATE NO ACTION

)ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;

DROP TABLE IF EXISTS Follow;
CREATE TABLE Follow(
id int(11) NOT NULL auto_increment,
followed_user_id int(11) NOT NULL,
following_user_id int(11) NOT NULL,
PRIMARY KEY (id),

CONSTRAINT FK_Followed_User FOREIGN KEY (followed_user_id) 
REFERENCES `User`(id)
ON DELETE CASCADE ON UPDATE NO ACTION,

CONSTRAINT FK_Following_User FOREIGN KEY (following_user_id) 
REFERENCES `User`(id)
ON DELETE CASCADE ON UPDATE NO ACTION

)ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;
